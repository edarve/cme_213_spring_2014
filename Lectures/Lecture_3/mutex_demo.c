#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

bool mutex_use;

void ProcessOpt(int argc, char **argv) {
  int c;
  mutex_use = false;
  while ((c = getopt(argc, argv, "mh")) != -1)
    switch (c) {
    case 'm':
      mutex_use = true;
      break;
    case 'h':
      printf("Options:\n-m\tUse a mutex\n");
      exit(1);
    case '?':
      break;
    }
}

typedef struct TaskList {
  struct TaskList * next;
  char * work_to_do;
} TaskList;

TaskList *task;

TaskList * InsertNewOrder(const char * order, TaskList * prev) {
  TaskList * new_task;
  new_task = (TaskList*) malloc(sizeof(TaskList));
  new_task->next = NULL;
  if (prev != NULL)
    prev->next = new_task;
  new_task->work_to_do = (char*) malloc(sizeof(char) * 80);
  strncpy(new_task->work_to_do, order, 80);
  puts(new_task->work_to_do);
  return new_task;
}

void delivery() {
  struct timespec req, rem;
  req.tv_sec = 0;
  req.tv_nsec = (1 + rand() % 10) * 100000000; /* Wait time in nanoseconds*/
  nanosleep(&req, &rem);
}

void * PizzaDeliveryPronto(void * tid) {
  long thread_id = (long) tid;
  TaskList *next_task;
  while (1) {
    if (mutex_use)
      pthread_mutex_lock(&mutex);
    if (task != NULL) {
      /* Pop a task */
      printf("Thread %ld: %s\n", thread_id, task->work_to_do);
      free(task->work_to_do);
      next_task = task->next;
      free(task);
      task = next_task;
      if (mutex_use)
        pthread_mutex_unlock(&mutex);
      /* At this point, the thread delivers the pizza. */
      /* The mutex is unlocked so that other threads can work. */
      delivery();
    } else {
      /* Done! Don't forget to unlock the mutex */
      if (mutex_use)
        pthread_mutex_unlock(&mutex);
      pthread_exit(NULL);
    }
  }
}

int main(int argc, char **argv) {
  TaskList *curr_task;
  int n_orders;
  long i;

  char order[80];

  ProcessOpt(argc, argv);

  if (mutex_use)
    printf("Using mutex to protect access\n\n");

  strncpy(order, "Open the store", 80);
  curr_task = InsertNewOrder(order, NULL);

  task = curr_task;

  /* Insert some tasks */
  n_orders = 8;
  for (i = 0; i < n_orders; ++i) {
    /* Got a phone call for pizza delivery. */
    snprintf(order, 80, "Pizza delivery to customer no. %ld", i + 1);
    curr_task = InsertNewOrder(order, curr_task);
  }

  strncpy(order, "Close the store", 80);
  curr_task = InsertNewOrder(order, curr_task);

  printf("\n");

  /* Have threads consume tasks */
  pthread_t thread[4]; /* Using 4 threads */
  for (i = 0; i < 4; ++i)
    pthread_create(thread + i, NULL, PizzaDeliveryPronto, (void *) i);

  for (i = 0; i < 4; ++i)
    pthread_join(thread[i], NULL);

  pthread_mutex_destroy(&mutex);

  printf("\nDone, Prof. Darve. Goodbye.\n");

  return 0;
}
