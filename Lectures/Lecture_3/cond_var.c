#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define NUMTASKS 4
#define NUMTHREADS 2 /* Must divide NUMTASKS */

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

typedef struct TaskList {
  struct TaskList * next;
  char * work_to_do;
} TaskList;

TaskList *task;

TaskList * InsertNewOrder(const char * order, TaskList * prev) {
  TaskList * new_task;
  new_task = (TaskList*) malloc(sizeof(TaskList));
  new_task->next = NULL;
  if (prev != NULL)
    prev->next = new_task;
  new_task->work_to_do = (char*) malloc(sizeof(char) * 80);
  strncpy(new_task->work_to_do, order, 80);
  printf("Inserting: %s\n", new_task->work_to_do);
  return new_task;
}

void delivery() {
  struct timespec req, rem;
  req.tv_sec = 0;
  req.tv_nsec = (1 + rand() % 10) * 100000000; /* Wait time in nanoseconds*/
  nanosleep(&req, &rem);
}

void *PizzaDeliveryPronto(void * tid) {
  int tasks_to_do = NUMTASKS / NUMTHREADS;
  long thread_id = (long) tid;
  TaskList *next_task;

  while (tasks_to_do--) {
    pthread_mutex_lock(&mutex);
    while (task == NULL) {
      printf("Consumer thread %ld: waiting for next job order.\n\n", thread_id);
      pthread_cond_wait(&cond, &mutex);
    }
    printf("Consumer thread %ld: %s\n", thread_id, task->work_to_do);
    free(task->work_to_do);
    next_task = task->next;
    free(task);
    task = next_task;
    pthread_mutex_unlock(&mutex);
    /* At this point, the thread delivers the pizza. */
    /* The mutex is unlocked so that other threads can work. */
    delivery();
  }

  printf("Consumer thread %ld: all done\n\n", thread_id);

  return NULL;
}

int main(int argc, char **argv) {
  pthread_t thread[NUMTHREADS];
  int n_orders = NUMTASKS;
  long i;
  TaskList *curr_task;
  char order[80];

  task = NULL;

  printf("Creating %d threads.\n", NUMTHREADS);
  for (i = 0; i < NUMTHREADS; ++i)
    pthread_create(&thread[i], NULL, PizzaDeliveryPronto, (void*) i);

  /* The producer loop */
  while (n_orders--) {
    printf("Producer: waiting for orders. Standing by the phone.\n\n");

    sleep(1);

    /* Received a call. */
    printf("Producer: locking shared data.\n");
    printf("Producer: received a phone call.\n");
    snprintf(order, 80, "Pizza delivery to customer ID %d", rand() % 1000);
    pthread_mutex_lock(&mutex); /* Protect shared data. */
    if (task == NULL) { /* Queue is empty */
      curr_task = InsertNewOrder(order, NULL);
      task = curr_task; /* New head of queue */
    } else
      curr_task = InsertNewOrder(order, curr_task);

    pthread_cond_signal(&cond); /* Wake up a consumer. */

    printf("Producer: unlocking shared data.\n");
    pthread_mutex_unlock(&mutex);
  }

  printf("Waiting for the consumer threads to complete.\n\n");
  for (i = 0; i < NUMTHREADS; ++i)
    pthread_join(thread[i], NULL);

  pthread_mutex_destroy(&mutex);
  pthread_cond_destroy(&cond);
  printf("A job well done!\n");

  return 0;
}
