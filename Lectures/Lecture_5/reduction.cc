#include <omp.h>
#include <vector>
using std::vector;

int main(void) {
  float average = 0.0;
  int size = 128;
  vector<float> a(size);
#pragma omp parallel for reduction (+:average)
  for (int i = 0; i < size; i++) {
    average += a[i];
  }
  average = average / size;
  return 0;
}
