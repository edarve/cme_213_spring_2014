#include <omp.h>

float LongCalculation() {
  return 0.;
}

int main(void) {
  float sum = 0.;
#pragma omp parallel
  {
    float result = LongCalculation();
#pragma omp atomic
    {
      sum += result;
    }
  }
  return 0;
}

