#include <omp.h>

float LongCalculation(int i) {
  return 0.;
}

void Consume(float r1, float & r2) {
  // Do some additional calculations beyond a simple +=
  r2 += r1;
}

int main(void) {
  float result = 0.;
  int niteration = 128;
#pragma omp parallel
  {
    int id = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    for (int i(id); i < niteration; i += nthreads) {
      float result_iterate = LongCalculation(i);
#pragma omp critical
      {
        Consume(result_iterate, result);
      }
    }
  }
  return 0;
}

