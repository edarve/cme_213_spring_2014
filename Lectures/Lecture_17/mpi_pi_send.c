#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

void srandom(unsigned seed);
double DartBoard(int darts);

#define DARTS 50000   /* number of throws at dartboard */
#define ROUNDS 10     /* number of times "darts" is iterated */
#define MASTER 0      /* task ID of master task */

int main(int argc, char *argv[]) {
  double	my_pi,      /* value of pi calculated by current task */
          pi,         /* average of pi after "darts" are thrown */
          avepi,      /* average pi value for all iterations */
          pirecv,     /* pi received from worker */
          pisum;      /* sum of workers pi values */
  int	taskid,         /* task ID - also used as seed number */
      numtasks,       /* number of tasks */
      tag,            /* message tag */
      rc,             /* return code */
      i, n;
  MPI_Status status;

  /* Obtain number of tasks and task ID */
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
  MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
  printf ("MPI task %d has started...\n", taskid);

  /* Set seed for random number generator equal to task ID */
  srandom (taskid);

  avepi = 0.;
  for (i = 0; i < ROUNDS; i++) {
    /* All tasks calculate pi using dartboard algorithm */
    my_pi = DartBoard(DARTS);

    /* Workers send my_pi to master */
    /* - Message tag is set to the iteration count */
    if (taskid != MASTER) {
      tag = i;
      rc = MPI_Send(&my_pi, 1, MPI_DOUBLE,
                    MASTER, tag, MPI_COMM_WORLD);
      if (rc != MPI_SUCCESS)
        printf("%d: Send failure on round %d\n", taskid, tag);

    } else {

      /* Master receives messages from all workers */
      /* - Message tag is equal to the iteration count */
      /* - Message source is set to the wildcard MPI_ANY_SOURCE: */
      /*   a message can be received from any task, as long as the */
      /*   message types match */
      tag = i;
      pisum = 0;
      for (n = 1; n < numtasks; n++) {
        rc = MPI_Recv(&pirecv, 1, MPI_DOUBLE, MPI_ANY_SOURCE,
                      tag, MPI_COMM_WORLD, &status);
        if (rc != MPI_SUCCESS)
          printf("%d: Receive failure on round %d\n", taskid, tag);

        /* running total of pi */
        pisum += pirecv;
      }

      /* Master calculates the average value of pi for this iteration */
      pi = (pisum + my_pi)/numtasks;
      /* Master calculates the average value of pi over all iterations */
      avepi = ((avepi * i) + pi)/(i + 1);
      printf("   After %8d throws, average value of pi = %10.8f\n",
             (DARTS * (i + 1) * numtasks),avepi);
    }
  }

  if (taskid == MASTER)
    printf ("\nReal value of PI: 3.1415926535897 \n");

  MPI_Finalize();

  return 0;
}
