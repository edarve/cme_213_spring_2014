#include <omp.h>
#include <cstdlib>

struct Node {
  int data;
  Node * next;
};

void Visit(Node * curr_node) {
  /* do work here */
}

void IncrementListItems(Node * head) {
#pragma omp parallel
  {
#pragma omp single
    {
      Node * curr_node = head;
      while (curr_node) {
#pragma omp task
        {
          // curr_node is firstprivate by default
          Visit(curr_node);
        }
        curr_node = curr_node->next;
      }
    }
  }
}

int main() {
  Node *root = new Node;

  int size = 10;

  // Fill the list
  Node * head = root;
  for (int i(0); i < size - 1; i++)
    head = head->next = new Node;
  head->next = NULL;

  IncrementListItems(root);
}

