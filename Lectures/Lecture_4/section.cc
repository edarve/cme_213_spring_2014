#include <omp.h>
#include <vector>

using std::vector;

int main() {
  int size = 1000;
  vector<float> a(size), b(size), c(size), d(size);

  // Some initializations
  for (int i(0); i < size; i++) {
    a[i] = i * 1.5;
    b[i] = i + 2;
  }

#pragma omp parallel
  {
#pragma omp sections
    {
#pragma omp section
      for (int i(0); i < size; i++)
        c[i] = a[i] + b[i];

#pragma omp section
      for (int i(0); i < size; i++)
        d[i] = a[i] * b[i];
    } // end of sections
  } // end of parallel section

  // Only 2 threads have work to do in this code.

  return 0;
}

