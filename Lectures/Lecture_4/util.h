#ifndef UTIL_H
#define UTIL_H

#include <sys/time.h>
#include <cstdio>
#include <algorithm>

int timeval_subtract(struct timeval *result, struct timeval *t2,
    struct timeval *t1);

void timeval_print(struct timeval *tv);

#endif
