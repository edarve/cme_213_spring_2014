#include <omp.h>
#include <cstdlib>
#include <cstdio>

bool InsertCond() {
  return float(rand()) / RAND_MAX < 0.9;
}

struct Node {
  int data;
  Node *left, *right;
};

void FillTree(int max_level, int level, Node * curr_node) {
  if (level < max_level) {
    curr_node->left = new Node;
    curr_node->left->left = curr_node->left->right = NULL;
    if (InsertCond())
      FillTree(max_level, level + 1, curr_node->left);

    curr_node->right = new Node;
    curr_node->right->left = curr_node->right->right = NULL;
    if (InsertCond())
      FillTree(max_level, level + 1, curr_node->right);
  }
}

void Visit(Node * curr_node) {
  /* do work here */
}

// Post-order traversal
int PostOrderTraverse(struct Node *curr_node) {
  int left = 0;
  int right = 0;

  if (curr_node->left)
#pragma omp task shared(left)
    left = PostOrderTraverse(curr_node->left);
  /* Default attribute for task constructs is firstprivate.
   firstprivate = private but value is initialized with the value that
   the corresponding original item has when the construct is encountered.
   */

  if (curr_node->right)
#pragma omp task shared(right)
  {
    right = PostOrderTraverse(curr_node->right);
  }

#pragma omp taskwait

  Visit(curr_node);
  return 1 + left + right;
}

int main() {
  Node *root = new Node;

  int nlevel = 10; // Maximum number of levels in the tree
  int level = 1;

  // Create a random tree
  FillTree(nlevel, level, root);

#pragma omp parallel
#pragma omp single // Only a single thread should execute this
  printf("Number of nodes in the tree: %d\n", PostOrderTraverse(root));
}
