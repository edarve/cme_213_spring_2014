#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <ctype.h>
#include <vector>
#include <unistd.h>
#include <cmath>
#include "util.h"
#include <omp.h>

using std::vector;

void ProcessOpt(int argc, char **argv, int * size, int * nthread) {
  int c;
  *size = 64;
  *nthread = 1;
  while ((c = getopt(argc, argv, "n:p:h")) != -1)
    switch (c) {
    case 'n':
      *size = atoi(optarg);
      break;
    case 'p':
      *nthread = atoi(optarg);
      break;
    case 'h':
      printf(
          "Options:\n-n SIZE\t\tMatrix size\n-p NTHREAD\tNumber of threads\n");
      exit(1);
    case '?':
      break;
    }
}

// A(i,j)
float MatA(int i, int j) {
  return cos(i * j);
}

// B(i,j)
float MatB(int i, int j) {
  return sin(i * j);
}

int main(int argc, char **argv) {
  // Command line options
  int size, nthread;
  ProcessOpt(argc, argv, &size, &nthread);

  assert(nthread >= 1 && nthread <= 64);
  assert(size >= 1);
  printf("Size of matrix = %d\n", size);
  printf("Number of threads to create = %d\n", nthread);

  // Setting the number of threads to use.
  // By default, openMP selects the largest possible number of threads given the processor.
  omp_set_num_threads(nthread);

  // Output matrix C
  vector<float> mat_c(size * size);

  // Begin
  struct timeval tvBegin, tvEnd, tvDiff; // For timing
  gettimeofday(&tvBegin, NULL);
  timeval_print(&tvBegin);

#pragma omp parallel for
  for (int i = 0; i < size; ++i)
    for (int j(0); j < size; ++j) {
      float c_ij = 0.;
      for (int k(0); k < size; ++k)
        c_ij += MatA(i, k) * MatB(k, j);
      mat_c[i * size + j] = c_ij;
      // mat_c is shared
    }

  // End
  gettimeofday(&tvEnd, NULL);
  timeval_print(&tvEnd);

  // Elapsed
  timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
  printf("Elapsed time [sec]: %ld.%06d\n", tvDiff.tv_sec, tvDiff.tv_usec);

  return 0;
}
