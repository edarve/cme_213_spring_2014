#include <omp.h>
#include <cstdio>
#include <cstdlib>
#include <ctime>

void Wait() {
  struct timespec req, rem;
  req.tv_sec = 0;
  req.tv_nsec = (1 + rand() % 10) * 1000; /* Wait time in nanoseconds*/
  nanosleep(&req, &rem);
}

int main() {

  int shared_int = -1;

#pragma omp parallel
  {
    // shared_int is shared
    int tid = omp_get_thread_num();
    printf("Thread ID %d: shared_int = %d\n", tid, shared_int);
  }

  int should_be_private = 0;
#pragma omp parallel private(should_be_private)
  {
    // should_be_private is private
    should_be_private = rand();
    int tid = omp_get_thread_num();
    printf("Thread ID %d: should_be_private = %d\n", tid, should_be_private);
  }

  printf("Done with parallel block; should_be_private = %d\n", should_be_private);

  int read_write = 0;
#pragma omp parallel
  {
    // At this point, the value of read_write is undefined.
    // This is a race condition.
    Wait();
    int tid = omp_get_thread_num();
    printf("Thread ID %d: read_write = %d\n", tid, read_write);
    ++read_write;
  }

  printf("read_write = %d\n", read_write);

  return 0;
}

