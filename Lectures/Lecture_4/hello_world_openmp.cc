#include <omp.h>
#include <cstdio>

void DoWork(int tid) {
  // busy work
  return;
}

int main() {
  // Fork a team of threads
#pragma omp parallel
  // This is like pthread_create(...)
  {
    // Obtain and print the thread ID
    int tid = omp_get_thread_num();
    printf("Hello World from thread = %d\n", tid);

    // Only thread 0 does this
    if (tid == 0) {
      int nthreads = omp_get_num_threads();
      printf("Number of threads = %d\n", nthreads);
    }

    // Pretend to do some work
    DoWork(tid);
  } // All threads join master thread and terminate

  return 0;
}

