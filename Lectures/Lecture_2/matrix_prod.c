#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <unistd.h>
#include <Accelerate/Accelerate.h>
#include "util.h"
#include <pthread.h>

void ProcessOpt(int argc, char **argv, int * size, int * nthread, bool * debug) {
  int c;
  *size = 64;
  *nthread = 1;
  *debug = true;
  while ((c = getopt(argc, argv, "n:p:dh")) != -1)
    switch (c) {
    case 'n':
      *size = atoi(optarg);
      break;
    case 'p':
      *nthread = atoi(optarg);
      break;
    case 'd':
      *debug = false;
      break;
    case 'h':
      printf(
          "Options:\n-n SIZE\t\tMatrix size\n-p NTHREAD\tNumber of threads\n");
      printf("-d\t\tCheck error in computation\n");
      exit(1);
    case '?':
      break;
    }
}

/* A(i,j) */
float MatA(int i, int j) {
  return cos(i * j);
}

/* B(i,j) */
float MatB(int i, int j) {
  return sin(i * j);
}

void InitializeMatrices(int size, float * mat_a, float * mat_b) {
  int i, j;
  for (i = 0; i < size; ++i)
    for (j = 0; j < size; ++j) {
      mat_a[i * size + j] = MatA(i, j);
      mat_b[i * size + j] = MatB(i, j);
    }
}

typedef struct {
  int size;
  int row_start, row_end;
  float *mat_c;
} MatrixType;

void * MatrixMult(void * w) {
  MatrixType *work;
  int size;
  float *mat_c, c_ij;

  int i, j, k;

  work = (MatrixType *) w;
  mat_c = work->mat_c;
  size = work->size;

  printf("Processing row %3d to %3d\n", work->row_start, work->row_end - 1);

  for (i = work->row_start; i < work->row_end; ++i)
    for (j = 0; j < size; ++j) {
      c_ij = 0.;
      for (k = 0; k < size; ++k)
        c_ij += MatA(i, k) * MatB(k, j);
      mat_c[i * size + j] = c_ij;
    }

  pthread_exit(NULL);
}

int main(int argc, char **argv) {
  int size;
  float *mat_a, *mat_b, *mat_c, *mat_d;
  int nthread; /* Number of threads to use */
  bool debug;
  MatrixType * work; /* Thread data */
  pthread_t *thread;

  struct timeval tvBegin, tvEnd, tvDiff; /* For timing */
  int i, nrow_per_thread;
  float norm_c, err;

  /* Command line options */
  ProcessOpt(argc, argv, &size, &nthread, &debug);
  assert(nthread >= 1 && nthread <= 64);
  assert(size >= 1);
  printf("Size of matrix = %d\n", size);
  printf("Number of threads to create = %d\n", nthread);

  thread = (pthread_t*) malloc(sizeof(pthread_t) * nthread);
  work = (MatrixType*) malloc(sizeof(MatrixType) * nthread);

  /* Initialize output matrix C */
  mat_c = (float*) malloc(sizeof(float) * size * size);

  /* Number of rows each thread will process */
  nrow_per_thread = (size + nthread - 1) / nthread;

  /* Begin */
  gettimeofday(&tvBegin, NULL);
  timeval_print(&tvBegin);

  /* Create all threads and do the computation */
  for (i = 0; i < nthread; ++i) {
    work[i].size = size;
    work[i].mat_c = mat_c;
    work[i].row_start = i * nrow_per_thread;
    work[i].row_end = min(size, (i + 1) * nrow_per_thread);
    pthread_create(thread + i, NULL, MatrixMult, (void *) (work + i));
  }

  /* Wait for threads to be done */
  for (i = 0; i < nthread; ++i)
    pthread_join(thread[i], NULL);

  /* End */
  gettimeofday(&tvEnd, NULL);
  timeval_print(&tvEnd);

  /* Elapsed */
  timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
  printf("Elapsed time [sec]: %ld.%06d\n", tvDiff.tv_sec, tvDiff.tv_usec);

  if (debug) {
    /* Debug: check result using BLAS GEMM routine */
    mat_a = (float*) malloc(sizeof(float) * size * size);
    mat_b = (float*) malloc(sizeof(float) * size * size);
    InitializeMatrices(size, mat_a, mat_b);
    mat_d = (float*) malloc(sizeof(float) * size * size);

    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, size, size, size,
        1.0, mat_a, size, mat_b, size, 0., mat_d, size);

    free(mat_b);
    free(mat_a);

    err = norm_c = 0.;
    for (i = 0; i < size * size; ++i) {
      norm_c = max(norm_c, fabs(mat_d[i]));
      err = max(err, fabs(mat_d[i] - mat_c[i]));
    }
    err /= norm_c;
    printf("Relative l_inf error: %g\n", err);
    assert(err < 1e-5);

    free(mat_d);
  }

  free(mat_c);
  free(work);
  free(thread);

  return 0;
}
