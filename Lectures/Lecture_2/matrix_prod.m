n = 4;

A = zeros(n);
B = A;

for i=1:n
    for j=1:n
        A(i,j) = 1/i;
        B(i,j) = 1/j;
    end
end
A*B