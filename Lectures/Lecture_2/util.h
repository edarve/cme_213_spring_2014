#ifndef UTIL_H
#define UTIL_H

#include <sys/time.h>
#include <stdio.h>

#define min(a,b) \
    ({ __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a < _b ? _a : _b; })

#define max(a,b) \
    ({ __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })

int timeval_subtract(struct timeval *result, struct timeval *t2,
    struct timeval *t1);

void timeval_print(struct timeval *tv);

#endif