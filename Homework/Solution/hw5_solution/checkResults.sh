# Usage: ./checkResult.sh [number_of_processes] [params_file.in] 
#!/bin/sh

if [ $# -lt 1 ]; then
    numpr=4
else
    numpr=$1 
fi
if [ $# -lt 2 ]; then
    params="params.in"
else
    params="$2"
fi

# Generate Reference File
./main $params
mv grid0_final.txt ref.txt

# Compare reference to individual pieces.
mpirun -np $numpr main $params
k=$(expr $numpr - 1)
for j in $(seq 0 $k)
do
    ./checker $j $numpr $params
done

rm -f *_init.txt *_final.txt ref.txt
