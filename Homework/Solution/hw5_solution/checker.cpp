#include <vector>
#include <iostream>
#include <fstream>
#include <iterator>
#include <sstream>
#include <string>
#include <utility>
#include <stack>
#include <cstdlib>
#include <cassert>

using namespace std;

string GetFileName(int rank) {
  stringstream ss;
  ss << "grid" << rank << "_final.txt";
  return ss.str();
}

void GetParams(const string& file, int *nx, int *ny, int *order) {
  ifstream ifs(file.c_str());
  if (!ifs.good()) {
    cout << "Couldn't open parameter file!" << endl;
    exit(1);
  }
  int nx_, ny_, iters_, order_, blocking_;
  double alpha_, lx_, ly_;
  ifs >> nx_ >> ny_;
  ifs >> lx_ >> ly_;
  ifs >> alpha_;
  ifs >> iters_;
  ifs >> order_;
  ifs >> blocking_;
  ifs.close();
  *nx = nx_;
  *ny = ny_;
  *order = order_;
}

vector<double> GetNumbers(const string& line, int gx) {
  vector<double> res;
  stringstream ss(line);
  int count = 0;
  double temp;
  while (ss >> temp) {
    ++count;
    res.push_back(temp);
  }
  assert(count == gx);
  return res;
}

vector<vector<double> > ReconstructGridFromFile(const string& filename, int rank, int numProcs, int nx, int ny, int order) {
  int numLines = ny / numProcs;
  int mod = ny % numProcs;
  if (rank < mod) {
    ++numLines;
  }
  int gx = nx + order;
  int gy = numLines + order;
  vector<vector<double> > res(gy);
  string line;
  ifstream f(filename.c_str());

  int idx = gy - 1;
  while (!f.fail() && idx >= 0) {
    getline(f, line);
    res[idx] = GetNumbers(line, gx);
    --idx;
  }
  assert(idx == -1);
  return res;
}

bool CheckResult(const vector<vector<double> >& ref, const vector<vector<double> >& cont, int rank, int numProcs, int nx, int ny, int order) {
  int mod = ny % numProcs;
  int numLines = ny / numProcs;
  int Yoffset = numLines * rank;
  int myGy = ny / numProcs + order;
  if (rank >= mod) {
    Yoffset += mod;
  } else {
    ++myGy;
    Yoffset += rank;
  }
  for (int i = 0; i < myGy; ++i) {
    for (int j = 0; j < nx + order; ++j) {
      // uncomment if you want to print elements
      //			cout << ref[Yoffset + i][j] << "        " << cont[i][j] << endl;
      if (ref[Yoffset + i][j] != cont[i][j]) {
        cout << "Error at (" << j << ", " << i << ") for process " << rank << "." << endl;
        return false;
      }
    }
  }
  return true;
}

int main(int argc, char** argv) {

  if (argc != 4) {
    cout << "Usage: " << argv[0] << " processor_rank num_processors params_file. " << endl;
    exit(1);
  }
  int rank = atoi(argv[1]);
  int numProcs = atoi(argv[2]);
  string paramsFile = argv[3];
  assert(rank < numProcs);

  int *nx, *ny, *order;
  nx = new int();
  ny = new int();
  order = new int();
  GetParams(paramsFile, nx, ny, order);

  vector<vector<double> > ref = ReconstructGridFromFile("ref.txt", 0, 1, *nx, *ny, *order);
  string fileToRead = GetFileName(rank);
  vector<vector<double> > cont = ReconstructGridFromFile(fileToRead, rank, numProcs, *nx, *ny, *order);

  bool correct = CheckResult(ref, cont, rank, numProcs, *nx, *ny, *order);
  if (!correct) {
    cout << "Oops, it seems that your result is wrong for process " << rank << ". Try again!" << endl;
  } else {
    cout << "Test is good for process " << rank;
    if (rank < numProcs - 1) {
      cout << ". Keep hoping..." << endl;
    } else cout << ". Sounds like victory ..." << endl;
  }
  return 0;
}

