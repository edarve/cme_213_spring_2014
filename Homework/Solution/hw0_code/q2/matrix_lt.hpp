#ifndef MATRIX_LT_HPP
#define MATRIX_LT_HPP

#include <algorithm>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "matrix.hpp"

template <typename T>
class MatrixLowerTriangular : public Matrix<T> {
 private:
   unsigned int n_;

   std::vector<T> data_;

 public:
  // Constructor takes argument n = matrix dimension.
  MatrixLowerTriangular(const unsigned int n);

  // Returns reference to matrix element (i,j). Throws exception if j > i.
  T& operator() (const unsigned int i, const unsigned int j);
  
  // Number of non-zero elements in matrix.
  T NormL0() const;
};

template <typename T>
MatrixLowerTriangular<T>::MatrixLowerTriangular(const unsigned int n) : n_(n) {
  data_.resize(n * (n + 1) / 2);
}

template <typename T>
T& MatrixLowerTriangular<T>::operator() (const unsigned int i, const unsigned int j) {
  if (i >= n_ or j >= n_)
    throw std::range_error("Index out of range.");
  if (i < j)
    throw std::range_error("Trying to access upper triangular part.");
  return data_[j + i * (i + 1) / 2];
}

template <typename T>
T MatrixLowerTriangular<T>::NormL0() const {
  auto is_nnz_fun = std::bind1st(std::not_equal_to<T>(), static_cast<T>(0));
  return std::count_if(data_.begin(), data_.end(), is_nnz_fun);
}

#endif /* MATRIX_LT_HPP */

