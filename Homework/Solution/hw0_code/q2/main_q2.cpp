#include <assert.h>

#include "matrix_lt.hpp" 

int main() {
 // Test 1: Initialize and test L0 norm
 try {
    MatrixLowerTriangular<double> matrix(100);
    if (matrix.NormL0() != 0) 
      throw std::runtime_error("L0 Norm calculation incorrect");
    matrix(1,1) = 4;
    matrix(10,1) = -3;
    if (matrix.NormL0() != 2) 
      throw std::runtime_error("L0 Norm calculation incorrect");
    std::cout << "L0Norm test passed." << std::endl;
  } catch (const std::exception& error) {
    std::cout << "Exception caught: " << error.what() << std::endl;
  }
  
  // Test 2: Initializing and retrieving values
  try {
    int n = 10;
    MatrixLowerTriangular<long> matrix(n);

    for (int i = 0; i < n; i++)
      for (int j = 0; j <= i; j++)
        matrix(i, j) = n * i + j;

    for (int i = 0; i < n; i++)
      for (int j = 0; j <= i; j++)
        if (matrix(i, j) != n * i + j)
          throw std::runtime_error("Retrieval failed");
    std::cout << "Initialization test passed." << std::endl;
  } catch (const std::exception& error) {
    std::cout << "Exception caught: " << error.what() << std::endl;
  }

  // Test 3: Accessing upper diagonal entries
  try {
    MatrixLowerTriangular<int> matrix(5);
    matrix(0, 3);
    std::cout << "Error: Accessing upper triangle did not fail." << std::endl;
  } catch (const std::exception& error) {
    std::cout << "Upper triangular access test passed." << std::endl;
  }

  // Test 4: Empty matrix
  try {
    MatrixLowerTriangular<float> matrix(0);
    if (matrix.NormL0() != 0) 
      throw std::runtime_error("Empty Matrix incorrect l0 norm.");
    std::cout << "Empty matrix test passed." << std::endl;
  } catch (const std::exception& error) {
    std::cout << "Exception caught: " << error.what() << std::endl;
  }

  // Test 5: Out of bounds 
  try {
    MatrixLowerTriangular<short> matrix(10);
    matrix(100, 1);
    std::cout << "Error: Out of bounds access did not fail." << std::endl;
  } catch (const std::exception& error) {
    std::cout << "Out of bounds test passed." << std::endl;
  }

  return 0;
}

