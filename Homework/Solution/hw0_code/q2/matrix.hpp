#ifndef MATRIX_HPP
#define MATRIX_HPP

template <typename T>
class Matrix {
 public:
  virtual T& operator() (const unsigned int i, const unsigned int j) = 0;
   
  virtual T NormL0() const = 0;

  virtual ~Matrix() = 0;
};

template <typename T>
Matrix<T>::~Matrix() {}

#endif /* MATRIX_HPP */

