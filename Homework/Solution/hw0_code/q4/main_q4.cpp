#include <iostream>
#include <random>
#include <set>
#include <stdexcept>

unsigned int count_range(const std::set<double> &data, const double lb, const double ub) {
  if (lb > ub)
    throw std::range_error("Lower bound must be less than upper bound");
  std::set<double>::const_iterator lb_it = data.lower_bound(lb);
  std::set<double>::const_iterator ub_it = data.upper_bound(ub);
  return std::distance(lb_it, ub_it);
}

int main() {
  // Test with N(0,1) data.
  std::cout << "Generating N(0,1) data" << std::endl;
  std::set<double> data;
  std::default_random_engine generator;
  std::normal_distribution<double> distribution(0.0, 1.0);
  for (unsigned int i = 0; i < 1000; ++i)
    data.insert(distribution(generator));
  std::cout << "> Number of elements in range [2, 10]: " << count_range(data, 2, 10) << std::endl;
}

