// HW0 solution
// Problem 1 
#include <stdlib.h>

void swap(double* &a, double* &b) {
  double* temp = a;
  a = b;
  b = temp;
}

void swap2(double** a, double** b) {
  double* temp = *a;
  *a = *b;
  *b = temp;
}

int main() {
  double* a = (double*) malloc(1000000 * sizeof(double));
  double* b = (double*) malloc(1000000 * sizeof(double));

  swap(a, b);
  swap2(&a, &b);

  free(a);
  free(b);
}

