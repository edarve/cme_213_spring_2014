#include <algorithm>

/* Each kernel handles the update of one pagerank score. In other 
 * words, each kernel handles one row of the update:
 * 
 *      pi(t+1) = A pi(t) + (1/2N)
 * 
 */
__global__
void device_graph_propagate(const uint *graph_indices
                          , const uint *graph_edges
                          , const float *graph_nodes_in
                          , float *graph_nodes_out
                          , const float *inv_edges_per_node
                          , int num_nodes)
{
    const int tid = blockIdx.x * blockDim.x + threadIdx.x;
    //the parallelization over nodes happens because we launch many threads

    if (tid >= num_nodes)
      return;

    float sum = 0.f;
    //for each edge
    uint low_bound = graph_indices[tid];
    uint high_bound = graph_indices[tid + 1];
    for (uint j = low_bound; j < high_bound; ++j) {
      uint node = graph_edges[j];
      sum += graph_nodes_in[ node ] * inv_edges_per_node[ node ];
    }
    graph_nodes_out[tid] = 0.5f/(float)num_nodes + 0.5f * sum;
}

/* This function executes a specified number of iterations of the
 * pagerank algorithm. The variables are:
 *
 * h_graph_indices, h_graph_edges: 
 *     These arrays describe the indices of the neighbors of node i.
 *     Specifically, node i is adjacent to all nodes in the range
 *     h_graph_edges[h_graph_indices[i] ... h_graph_indices[i+1]].
 *     
 * h_node_values_input:
 *     An initial guess of pi(0).
 *
 * h_gpu_node_values_output:
 *     Output array for the pagerank vector.
 *
 * h_inv_edges_per_node:
 *     The i'th element in this array is the reciprocal of the 
 *     out degree of the i'th node.
 * 
 * nr_iterations:
 *     The number of iterations to run the pagerank algorithm for.
 * 
 * num_nodes: 
 *     The number of nodes in the whole graph (ie N).
 *
 * avg_edges:
 *     The average number of edges in the graph. You are guaranteed
 *     that the whole graph has num_nodes * avg_edges edges.
 */
double device_graph_iterate(const uint *h_graph_indices
                          , const uint *h_graph_edges
                          , const float *h_node_values_input
                          , float *h_gpu_node_values_output
                          , const float *h_inv_edges_per_node
                          , int nr_iterations
                          , int num_nodes
                          , int avg_edges)
{
  uint *d_graph_indices=0;
  uint *d_graph_edges=0;
  float *d_graph_nodes_A=0;
  float *d_graph_nodes_B=0;
  float *d_inv_edges_per_node=0;

  cudaMalloc(&d_graph_indices,     (num_nodes + 1)        * sizeof(uint));
  cudaMalloc(&d_graph_edges,        num_nodes * avg_edges * sizeof(uint));
  cudaMalloc(&d_graph_nodes_A,      num_nodes             * sizeof(float));
  cudaMalloc(&d_graph_nodes_B,      num_nodes             * sizeof(float));
  cudaMalloc(&d_inv_edges_per_node, num_nodes             * sizeof(float));

  if (d_graph_indices == 0 || d_graph_edges == 0 || d_graph_nodes_A == 0||
      d_graph_nodes_B == 0 || d_inv_edges_per_node == 0) {
      std::cerr << "Couldn't allocate enough memory on the GPU!" << std::endl;
      std::cerr << "num nodes: " << num_nodes << " avg_edges: " << avg_edges << std::endl;
      exit(1);
  }

  cudaMemcpy(d_graph_indices, h_graph_indices,          
             (num_nodes + 1) * sizeof(uint), cudaMemcpyHostToDevice);
  cudaMemcpy(d_graph_edges,   h_graph_edges, 
             num_nodes * avg_edges * sizeof(uint), cudaMemcpyHostToDevice);
  cudaMemcpy(d_graph_nodes_A, h_node_values_input,
             num_nodes * sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_inv_edges_per_node, h_inv_edges_per_node, 
             num_nodes * sizeof(float), cudaMemcpyHostToDevice);

  start_timer(&timer);

  const int block_size = 192;
  
  for (int i = 0; i < nr_iterations / 2; i ++) {
      device_graph_propagate<<<(num_nodes + block_size - 1) / block_size, block_size>>>(
              d_graph_indices, d_graph_edges, d_graph_nodes_A, d_graph_nodes_B, d_inv_edges_per_node, num_nodes);
      device_graph_propagate<<<(num_nodes + block_size - 1) / block_size, block_size>>>(
              d_graph_indices, d_graph_edges, d_graph_nodes_B, d_graph_nodes_A, d_inv_edges_per_node, num_nodes);

  }

  if (nr_iterations % 2) {
      device_graph_propagate<<<(num_nodes + block_size - 1) / block_size, block_size>>>(
              d_graph_indices, d_graph_edges, d_graph_nodes_A, d_graph_nodes_B, d_inv_edges_per_node, num_nodes);
      std::swap(d_graph_nodes_A, d_graph_nodes_B);
  }
  
  check_launch("gpu graph propagate");
  double gpu_elapsed_time = stop_timer(&timer);

  cudaMemcpy(h_gpu_node_values_output, d_graph_nodes_A, num_nodes * sizeof(float), cudaMemcpyDeviceToHost);

  cudaFree(d_graph_indices);
  cudaFree(d_graph_edges);
  cudaFree(d_graph_nodes_A);
  cudaFree(d_graph_nodes_B);
  cudaFree(d_inv_edges_per_node);

  return gpu_elapsed_time;
}
