#include <algorithm>

// Repeating from the tutorial, just in case you haven't looked at it.

// "kernels" or __global__ functions are the entry points to code that executes on the GPU
// The keyword __global__ indicates to the compiler that this function is a GPU entry point.
// __global__ functions must return void, and may only be called or "launched" from code that
// executes on the CPU.

// This kernel implements a per element shift
// by naively loading one byte and shifting it
__global__ void shift_char(const unsigned char *input_array
                                , unsigned char *output_array
                                , unsigned char shift_amount
                                , unsigned int array_length)
{
    const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;

    for (unsigned int i = tid; i < array_length; i += gridDim.x * blockDim.x)
        output_array[i] = input_array[i] + shift_amount;
}

//Here we load 4 bytes at a time instead of just 1
//to improve the bandwidth due to a better memory
//access pattern
__global__ void shift_int(const unsigned int *input_array
                               , unsigned int *output_array
                               , unsigned int shift_amount
                               , unsigned int array_length)
{
    const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;

    for (unsigned int i = tid; i < array_length; i += gridDim.x * blockDim.x)
        output_array[i] = input_array[i] + shift_amount;
}

//Here we go even further and load 8 bytes
//does it make a further improvement?
__global__ void shift_int2(const uint2 *input_array
                                , uint2 *output_array
                                , unsigned int shift_amount
                                , unsigned int array_length)
{
    const int tid = blockIdx.x * blockDim.x + threadIdx.x;

    for (unsigned int i = tid; i < array_length; i += gridDim.x * blockDim.x) {
        output_array[i].x = input_array[i].x + shift_amount;
        output_array[i].y = input_array[i].y + shift_amount;
    }
}

//the following three kernels launch their respective kernels
//and report the time it took for the kernel to run

double doGPUShiftChar(const unsigned char *d_input
                    , unsigned char *d_output
                    , unsigned char shift_amount
                    , unsigned int text_size
                    , unsigned int block_size)
{
    int grid_size = std::min((text_size + block_size - 1) / block_size, 65535u);
    event_pair timer;
    start_timer(&timer);
    // launch kernel
    shift_char<<<grid_size, block_size>>>(d_input, d_output, shift_amount, text_size);
    check_launch("gpu shift cipher char");
    return stop_timer(&timer);
}

double doGPUShiftUInt(const unsigned char *d_input
                    , unsigned char *d_output
                    , unsigned char shift_amount
                    , unsigned int text_size
                    , unsigned int block_size)
{
    int grid_size = std::min(((text_size + 3) / 4 + block_size - 1) / block_size, 65535u);
    unsigned int iShift = (shift_amount | (shift_amount << 8) | (shift_amount << 16) | (shift_amount << 24));
    event_pair timer;
    start_timer(&timer);
    // launch kernel
    shift_int<<<grid_size, block_size>>>((unsigned int *)d_input, (unsigned int *)d_output, iShift, (text_size + 3) / 4);
    check_launch("gpu shift cipher uint");
    return stop_timer(&timer);
}

double doGPUShiftUInt2(const unsigned char *d_input
                     , unsigned char *d_output
                     , unsigned char shift_amount
                     , unsigned int text_size
                     , unsigned int block_size)
{
    int grid_size = std::min(((text_size + 7) / 8 + block_size - 1) / block_size, 65535u);

    unsigned int iShift = (shift_amount | (shift_amount << 8) | (shift_amount << 16) | (shift_amount << 24));
    event_pair timer;
    start_timer(&timer);
    // launch kernel
    shift_int2<<<grid_size, block_size>>>((uint2 *)d_input, (uint2 *)d_output, iShift, (text_size + 7) / 8);
    check_launch("gpu shift cipher uint2");
    return stop_timer(&timer);
}
