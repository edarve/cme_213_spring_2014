#include <algorithm>
#include <fstream>
#include <iostream>
#include <string> 
#include <vector>

#include <assert.h>
#include <omp.h>
#include <stdlib.h>

#include "test_macros.h"

#ifndef TESTS_H
#define TESTS_H

typedef unsigned int uint;

void TestCorrectness(const std::vector<uint>& reference, const std::vector<uint>& toTest);

void WriteVectorToFile(const std::string& filename, std::vector<uint>& v);

std::vector<uint> ReadVectorFromFile(const std::string& filename); 

uint StringToUint(const std::string& line);

void Test1();

void Test2();

void Test3();

void Test4();

void Test5();

#endif /* TESTS_H */
