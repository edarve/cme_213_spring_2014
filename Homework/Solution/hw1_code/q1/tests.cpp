#include <algorithm>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include "omp.h"
#include "tests.h"
#include <sstream>

uint StringToUint(const std::string& line) {
	std::stringstream buffer;
	uint res;
	buffer << line;
	buffer >> res;
	return res;
}

std::vector<uint> ReadVectorFromFile(const std::string& filename) { 
	std::ifstream infile(filename.c_str());
	if (!infile) std::cerr << "Failed to load the file." << std::endl;
	std::vector<uint> res;
	std::string line;
	while (true) {
		getline(infile, line);
		if (infile.fail()) break;
		res.push_back(StringToUint(line));	
	}
	return res;	
}

