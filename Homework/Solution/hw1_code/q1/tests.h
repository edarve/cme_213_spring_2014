#include <algorithm>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include "omp.h"
#include <fstream>
#include <string> 


#ifndef TESTS_H
#define TESTS_H

typedef unsigned int uint;

std::vector<uint> ReadVectorFromFile(const std::string& filename); 

uint StringToUint(const std::string& line);

#endif
