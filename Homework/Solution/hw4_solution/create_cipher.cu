#include <algorithm>
#include <cctype>
#include <fstream>
#include <iostream>
#include <vector>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/transform.h>
#include <thrust/remove.h>
#include <thrust/random.h>
#include <thrust/iterator/constant_iterator.h>

#include "test_macros.h"

struct isnot_lowercase_alpha : thrust::unary_function<unsigned char, bool> {
  __host__ __device__
  bool operator()(const unsigned char &c) {
    return c < 97 || c > 122;
  }
};

struct upper_to_lower : thrust::unary_function<unsigned char, unsigned char> {
  __host__ __device__
  unsigned char operator()(const unsigned char &c) {
    if (c >= 65 && c <= 90)
      return c + 32;
    else
      return c;
  }
};

struct apply_shift : thrust::binary_function<unsigned char, int,
                                             unsigned char> {
  unsigned int period;
  unsigned int *shifts;

  __host__ __device__
  apply_shift(unsigned int p, unsigned int *s) : period(p), shifts(s) {}

  __host__ __device__
  unsigned char operator()(const unsigned char &c, int pos) {
    unsigned char new_c = c + shifts[pos % period];
    if (new_c > 122)
      new_c -= 26;
    else if (new_c < 97)
      new_c += 26;
    return new_c;
  }
};

std::vector<double> getLetterFrequencyCpu(
    const std::vector<unsigned char> &text) {
  std::vector<unsigned int> freq(256);
  for (unsigned int i = 0; i < text.size(); ++i)
    freq[tolower(text[i])]++;

  unsigned int sum_chars = 0;
  for (unsigned char c = 'a'; c <= 'z'; ++c)
    sum_chars += freq[c];

  std::vector<double> freq_alpha_lower;
  for (unsigned char c = 'a'; c <= 'z'; ++c) {
    if (freq[c] > 0)
      freq_alpha_lower.push_back(freq[c] / static_cast<double>(sum_chars));
  }

  std::sort(freq_alpha_lower.begin(), freq_alpha_lower.end(),
            std::greater<double>());
  freq_alpha_lower.resize(min(static_cast<int>(freq_alpha_lower.size()), 5));
  return freq_alpha_lower;
}


// Print the top 5 letter frequencies and return top 5.
std::vector<double> getLetterFrequencyGpu(
    const thrust::device_vector<unsigned char> &text) {
  // first calculate letter frequency
  thrust::device_vector<unsigned char> text_copy(text);

  thrust::sort(text_copy.begin(), text_copy.end());

  thrust::device_vector<unsigned int> letterHisto(26);
  thrust::device_vector<unsigned char> letters(26);

  thrust::pair<thrust::device_vector<unsigned char>::iterator,
      thrust::device_vector<unsigned int>::iterator> outputIt =
      thrust::reduce_by_key(text_copy.begin(), text_copy.end(),
          thrust::make_constant_iterator(static_cast<unsigned int>(1)),
                                         letters.begin(),
                                         letterHisto.begin());

  int numLetters = outputIt.first - letters.begin();

  //make sure resize in case there are less than 26 letters present
  letters.resize(numLetters);
  letterHisto.resize(numLetters);

  thrust::sort_by_key(letterHisto.begin(), letterHisto.end(),
                      letters.begin(), thrust::greater<int>());

  // make sure we handle the absurd case of text containing less than
  // 5 distinct letters
  if (numLetters < 5)
    std::cout << "All " << numLetters
              << " Letter Frequencies:\n-------------" << std::endl;
  else
    std::cout << "Top " << 5
              << " Letter Frequencies:\n-------------" << std::endl;

  std::vector<double> freq_alpha_lower(min(5, numLetters));
  for (int i = 0; i < min(5, numLetters); ++i) {
    unsigned char c = letters[i];
    double freq = letterHisto[i] / static_cast<double>(text_copy.size());
    std::cout << c << " " << freq << std::endl;
    freq_alpha_lower[i] = freq;
  }
  return freq_alpha_lower;
}

int main(int argc, char **argv) {
  if (argc != 3) {
    std::cerr << "Didn't supply plain text and period!" << std::endl;
    return 1;
  }

  std::ifstream ifs(argv[1], std::ios::binary);
  if (!ifs.good()) {
    std::cerr << "Couldn't open book file!" << std::endl;
    return 1;
  }

  unsigned int period = atoi(argv[2]);

  if (period < 4) {
    std::cerr << "Period must be at least 4!" << std::endl;
    return 1;
  }

  std::vector<unsigned char> text;

  ifs.seekg(0, std::ios::end); // seek to end of file
  int length = ifs.tellg();    // get distance from beginning
  ifs.seekg(0, std::ios::beg); // move back to beginning

  text.resize(length);
  ifs.read((char *)&text[0], length);

  ifs.close();


  // sanitize input to contain only a-z lowercase
  thrust::device_vector<unsigned char> dText = text;
  thrust::device_vector<unsigned char> text_clean(text.size());

  int numElements = thrust::remove_copy_if(
      thrust::make_transform_iterator(dText.begin(), upper_to_lower()),
      thrust::make_transform_iterator(dText.end(), upper_to_lower()),
      text_clean.begin(), isnot_lowercase_alpha()) - text_clean.begin();

  text_clean.resize(numElements);

  std::cout << "\nBefore ciphering!" << std::endl << std::endl;

  std::vector<double> letterFreqGpu = getLetterFrequencyGpu(text_clean);
  std::vector<double> letterFreqCpu = getLetterFrequencyCpu(text);
  bool success = true;
  EXPECT_VECTOR_EQ_EPS(letterFreqCpu, letterFreqGpu, 1e-14, &success);
  PRINT_SUCCESS(success);

  thrust::device_vector<unsigned int> shifts(period);
  thrust::default_random_engine rng(123);
  // don't allow 0 shifts
  thrust::uniform_int_distribution<int> uniform_dist(1, 25);

  for (unsigned int i = 0; i < shifts.size(); ++i)
    shifts[i] = uniform_dist(rng);

  std::cout << "\nEncryption key: ";
  for (unsigned int i = 0; i < period; ++i)
    std::cout << static_cast<char>('a' + shifts[i]);
  std::cout << std::endl;

  thrust::device_vector<unsigned char> device_cipher_text(numElements);

  thrust::transform(text_clean.begin(), text_clean.begin() + numElements,
       thrust::make_counting_iterator(static_cast<int>(0)),
       device_cipher_text.begin(),
       apply_shift(period, thrust::raw_pointer_cast(&shifts[0])));


  thrust::host_vector<unsigned char> host_cipher_text = device_cipher_text;

  std::cout << "\nAfter ciphering!" << std::endl << std::endl;
  getLetterFrequencyGpu(device_cipher_text);

  std::ofstream ofs("cipher_text.txt", std::ios::binary);
  ofs.write((char *)&host_cipher_text[0], numElements);

  ofs.close();

  return 0;
}
