#ifndef STRIDED_RANGE_ITERATOR_H_
#define STRIDED_RANGE_ITERATOR_H_

//from thrust strided range example
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
/**
 * strided_range is an iterator wrapper, which takes as parameters
 * first, last and stride, and becomes an iterator for the elements
 *
 * first, first + stride, first + 2 * stride, ..., end
 *
 * where end = first + ((last - first) / stride) * stride.
 */
template <typename Iterator>
class strided_range {
  public:

    typedef typename thrust::iterator_difference<Iterator>::type
        difference_type;

    struct stride_functor : public thrust::unary_function<difference_type, 
                                                          difference_type> {
      difference_type stride;

      stride_functor(difference_type stride) : stride(stride) {}

      __host__ __device__
      difference_type operator()(const difference_type& i) const { 
        return stride * i;
      }
    };

    typedef typename thrust::counting_iterator<difference_type> 
        CountingIterator;
    typedef typename thrust::transform_iterator<stride_functor,
        CountingIterator> TransformIterator;
    typedef typename thrust::permutation_iterator<Iterator, TransformIterator>
        PermutationIterator;

    // type of the strided_range iterator
    typedef PermutationIterator iterator;

    // construct strided_range for the range [first,last)
    strided_range(Iterator first, Iterator last, difference_type stride)
      : first(first), last(last), stride(stride) {}

    iterator begin(void) const {
      return PermutationIterator(first,
          TransformIterator(CountingIterator(0), stride_functor(stride)));
    }

    iterator end(void) const {
      return begin() + ((last - first) + (stride - 1)) / stride;
    }

  protected:
    difference_type stride;
    Iterator first;
    Iterator last;
};

#endif /* STRIDED_RANGE_ITERATOR_H_ */

