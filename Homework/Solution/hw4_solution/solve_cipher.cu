#include <vector>
#include <fstream>
#include <iostream>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/reduce.h>
#include <thrust/inner_product.h>
#include <thrust/iterator/constant_iterator.h>

#include "strided_range_iterator.h"

struct apply_shift : thrust::binary_function<unsigned char, int,
                                             unsigned char> {
  unsigned int period;
  int *shifts;

  __host__ __device__
  apply_shift(unsigned int p, int *s) : period(p), shifts(s) {}

  __host__ __device__
  unsigned char operator()(const unsigned char &c, int pos) {
    unsigned char new_c = c + shifts[pos % period];
    if (new_c > 122)
      new_c -= 26;
    else if (new_c < 97)
      new_c += 26;
    return new_c;
  }
};

int main(int argc, char **argv) {
  if (argc != 2) {
    std::cerr << "No cipher text given!" << std::endl;
    return 1;
  }

  // First load the text
  std::ifstream ifs(argv[1], std::ios::binary);

  if (!ifs.good()) {
    std::cerr << "Couldn't open book file!" << std::endl;
    return 1;
  }

  std::vector<unsigned char> text;

  ifs.seekg(0, std::ios::end); // seek to end of file
  int length = ifs.tellg();    // get distance from beginning
  ifs.seekg(0, std::ios::beg); // move back to beginning

  text.resize(length);
  ifs.read((char *)&text[0], length);

  ifs.close();

  // we assume the cipher text has been sanitized
  thrust::device_vector<unsigned char> text_clean = text;

  // now we crack the Vigenere cipher
  // first we need to determine the key length
  // use the kappa index of coincidence
  int keyLength = 0;
  {
    bool found = false;
    int shift_idx = 4; // Start at index 4.
    while (!found) {
      int numMatches = thrust::inner_product(text_clean.begin() + shift_idx,
          text_clean.end(), text_clean.begin(), 0, thrust::plus<int>(),
          thrust::equal_to<unsigned char>());

      double ioc = numMatches /
          (static_cast<double>(text_clean.size() - shift_idx) / 26.);

      std::cout << "Period " << shift_idx << " ioc: " << ioc << std::endl;
      if (ioc > 1.6) {
        if (keyLength == 0) {
          keyLength = shift_idx;
          shift_idx = 2 * shift_idx - 1; // check double the period to make sure
        } else if (2 * keyLength == shift_idx) {
          found = true;
        } else {
          std::cout << "Unusual pattern in text! Probably period is < 4."
                    << std::endl;
          exit(1);
        }
      }
      ++shift_idx;
    }
  }

  std::cout << "keyLength: " << keyLength << std::endl;

  // once we know the key length, then we can do frequency analysis on each
  // pos mod length
  // allowing us to easily break each cipher independently
  thrust::device_vector<unsigned char> text_copy = text_clean;
  thrust::device_vector<int> dShifts(keyLength);
  typedef thrust::device_vector<unsigned char>::iterator Iterator;

  for (int i = 0; i < keyLength; ++i) {
    strided_range<Iterator> it(text_copy.begin() + i, text_copy.end(),
                               keyLength);
    thrust::sort(it.begin(), it.end());

    thrust::device_vector<int> letterHisto(26);
    thrust::device_vector<unsigned char> letters(26);
    // here we assume all 26 letters appear in the text, which is probably
    // a decent assumption
    thrust::reduce_by_key(it.begin(), it.end(),
                          thrust::make_constant_iterator(static_cast<int>(1)),
                          letters.begin(), letterHisto.begin());
    int maxLoc = thrust::max_element(letterHisto.begin(),
                                     letterHisto.end()) - letterHisto.begin();
    // Shift by 4 since we expect the max location to occur at 'e' (i.e., 4).
    dShifts[i] = -(maxLoc - 4);
  }

  std::cout << "\nEncryption key: ";
  for (int i = 0; i < keyLength; ++i)
    std::cout << static_cast<char>('a' - (dShifts[i] <= 0 ? dShifts[i] :
                                   dShifts[i] - 26));
  std::cout << std::endl;

  // take the shifts and transform cipher text back to plain text
  thrust::transform(text_clean.begin(), text_clean.end(),
      thrust::make_counting_iterator(static_cast<int>(0)), text_clean.begin(),
      apply_shift(keyLength, thrust::raw_pointer_cast(&dShifts[0])));

  thrust::host_vector<unsigned char> h_plain_text = text_clean;

  std::ofstream ofs("plain_text.txt", std::ios::binary);
  ofs.write((char *)&h_plain_text[0], h_plain_text.size());
  ofs.close();

  return 0;
}
