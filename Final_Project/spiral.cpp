// Generates spiral input file for CCL.

#include <cmath>
#include <cstdlib>
#include <fstream> 
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

#include "imghelpers.h"

typedef unsigned int uint;

std::vector<uint> CreateSpiral1D(uint n) {
	if (n % 2 == 0) { 
		std::cout << "Warning: the size you provided was even, we rounded it up to the next odd integer (" << n + 1 << ")" <<  std::endl;
		++n;
	}
	uint k = n / 2;
	int numIter = (int)k + 1; 
	uint flip = 1;
	std::vector<uint> v(n * n);
	for (int it = 0; it < numIter; ++it) { 
		int len = (int)n - 2 * it;
		for (int i = it; i < it + len; ++i) {
			v[i * n + it] = flip;
			v[i * n + n - 1 - it] = flip;
			v[it* n + i] = flip;
			v[(n - 1 - it) * n + i] = flip;
		}
		v[(it + 1) * n + it] = 1 - flip;
		flip = 1 - flip;
	}
	return v;
}


int main(int argc, char** argv){
	std::string outfile = "spiral.in";
	if (argc < 2) { 
		printf("Usage: %s size [filename_out.in]\n", argv[0]);
    exit(1);
	}
	if (argc > 2) { 
		outfile = argv[2];
	}
  int size = atoi(argv[1]);
	std::vector<uint> spiral = CreateSpiral1D(size);
	uint finalSize = (uint)sqrt(spiral.size());
	WriteImgToFile(outfile, spiral, finalSize, finalSize);
	return 0;
}
